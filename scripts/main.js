$(document).ready(function () {
    
    initMenuLinks();
    initModals();

})

function initModals () {
    const link = document.querySelector(".login-link");
    const popup = document.querySelector(".modal-login");
    const close = popup.querySelector(".modal-close");
    const form = popup.querySelector("form");
    const login = popup.querySelector("[name='login']");
    const password = popup.querySelector("[name='password']");
    const storage = localStorage.getItem("login");

    const mapLink = document.querySelector(".contacts-button-map");
    const mapPopup = document.querySelector(".modal-map");

    link.addEventListener("click", function (evt) {
        evt.preventDefault();
        openModal(popup)
    
        if (storage) {
            login.value = storage;
            password.focus();
        } else {
            login.focus();
        }
    });
    
    close.addEventListener("click", function (evt) {
        closeModal(evt, popup, mapPopup);
    });


    form.addEventListener("submit", function (evt) {
        if (!login.value || !password.value) {
            evt.preventDefault();
            popup.classList.add("modal-error");
            console.log("Нужно ввести логин и пароль");
        } else {
            if (storage === null) {
                console.log('form is full')
                localStorage.setItem("login", login.value);
            }
        }
    });

    window.addEventListener("keydown", function (evt) {
        if (evt.keyCode === 27) { 
            if (popup.classList.contains("modal-show")) {
                closeModal(evt, popup, mapPopup)
            }
        }
    });

    if (mapPopup) {
        const mapClose = mapPopup.querySelector(".modal-close");
        mapLink.addEventListener("click", function (evt) {
            evt.preventDefault();
            openModal(mapPopup)
        });

        mapClose.addEventListener("click", function (evt) {
            evt.preventDefault();
            mapPopup.classList.remove("modal-show");
        });

        window.addEventListener("keydown", function (evt) {
            if (evt.keyCode === 27) { 
                if (mapPopup.classList.contains("modal-show")) {
                    closeModal(evt, popup, mapPopup)
                }
            }
        });
    }
}

function openModal (popup) {
    popup.classList.add("modal-show");
}

function closeModal(evt, popup, mapPopup) {
    evt.preventDefault();
    popup.classList.remove("modal-show");  
    popup.classList.remove("modal-error");
    mapPopup.classList.remove("modal-show");
}

function initMenuLinks() {
    $("#menu").on("click", "a", function (e) {
        e.preventDefault();
        var linkHref  = $(this).attr('href')
        if (linkHref[0] !== '#') {
            window.location.href = linkHref
        } else if (document.querySelector(linkHref)) {
            var top = $(linkHref).offset().top;
            $('body, html').animate({scrollTop: top}, 600);
        } else {
            window.location.href = 'index.html' + linkHref
        }
    })
}

function app() {
    const input = document.querySelectorAll('.filter-input') 
    const li = document.querySelectorAll('.catalog-item')

    function filer (category, items) {
        items.forEach((item) => {
             const isItemFiltered = !item.classList.contains(category)
             if (isItemFiltered) {
                 item.classList.add('hide')
             }
        })
    }

    input.forEach((input) => {
        input.addEventListener('click', () => {
            const currentCategory = button.dataset.filer 
            filter(currentCategory, li)
            console.log()
        })
    })
}

app()